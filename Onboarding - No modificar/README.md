# TOBS Onboarding 

## Descripción

Esta carpeta tiene VTEX Legacy desde 0 para poder volver al inicio si se llega al punto en el que el sitio no da para mas

## Tareas del Onboarding

### Temario

- [] ¿Que es VTEX?
- [] CMS - ¿Que es? - https://help.vtex.com/es/tutorial/what-is-cms
- [] Sites and channel - General
- [] Sites and channel - Layouts - ¿Que son? - https://help.vtex.com/es/tutorial/que-son-layouts--CckPh00rZIcIUG60y8Gse
- [] Sites and channel - Propiedades
- [] Sites and channel - Settings 
- [] Sites and channel - Edit
- [] Templates - ¿Que son? - https://help.vtex.com/es/tutorial/what-are-templates


### Workflow de un sitio Ecommerce

- 1) HOME
- 2) Grilla de productos (Categorias, colecciones, resultados de busqueda)
- 3) Pagina de producto
- 4) Checkout

### Metodologia de Trabajo.

*Freedcamp*
- 1) Poner la tarea en progreso.
- 2) Asignarme la tarea
- 3) Setearle un due date
- 4) Setearle un story point
- 4) Enviar un comentario de que la tarea se va a empezar.

*Repositorio: Parte1*

    **Preparar el entorno de trabajo PREVIO a empezar la tarea.**

    - 1) Nos paramos en master (git checkout master)
    - 2) Actualizamos la rama master (git pull origin master)
    - 3) Creamos nuestra rama: Al estar parados en master, la nueva rama será una copia de master. (git checkout -b "id_NombreDeLaTarea")

    - Antes de empezar cada tarea, debemos tener los archivos del repositorio actualizado ya que nos sirve como backup ante imprevistos.
      Esto significa que tenemos copiar los archivos que iremos a usar (HTML,CSS,JS), tanto los de producción como los de prueba, desde VTEX al repo para que tengamos el codigo al dia.
      Por ejemplo: Supongamos que tenemos que trabajar sobre la home del sitio para cambiar un color .
      Lo primero que tenemos que hacer es detectar cual es el template que está usando producción, para eso vamos a ir a sites and channel y vamos a ubicar cual es la home de producción (la que tiene la tilde) y que template usa (EJ: home.html).
      Lo segundo que hay que hacer es preparar el template de prueba para usar el lid de prueba (EJ: home-test.html) que vamos a usar.  Una vez hecho esto, tendremos que copiarnos esos dos templates en sus respectivos lugares en el visual studio. Luego de que tengamos todos los archivos al dia y podamos visualizar el LID igual al sitio en producción, recien ahi haremos los siguientes pasos.


*Realizar la tarea*

*Repositorio: Parte 2*


### ¿Como verifico que es lo que tengo que modificar?

- 1) Sites and channel
- 2) Templates
- 3) Subtemplates
- 4) Archivos



### Tareas de creación

**Creación**

- [] Crear un template 
- [] Crear un Layout (Para la home)
- [] Crear un subtemplate
- [] Crear un shelve template
- [] Crear un custom Element

- [] Crear una colección
- [] Crear una landing

**Agregar elementos al sitio**

- [] Agregar un controlador de VTEX para el banner
- [] Agregar un Custom Element
- [] Agregar el contador de los eventos especiales


